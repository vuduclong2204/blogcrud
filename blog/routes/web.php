<?php

use App\Http\Controllers\CategoryController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Route::get('/', 'HomeController@getIndex');
Route::get('/creatblog','BlogController@getViewCreateBlog');
Route::get('/blogs', 'BlogController@getBlog')->name('blogs');
Route::post('/storeBlog','BlogController@getSubmitBlog');
Route::get('/editblog/{slug}','BlogController@getEditBlog');
Route::put('/update/{slug}','BlogController@getUpdateBlog');
Route::get('/delete/{slug}','BlogController@getDeleteBlog');
Route::get('/search','Blogcontroller@getSearch');
Route::get('/view-blog/{slug}','BlogController@ViewBlog');

/**
 * route category
 */
Route::get('/createCategory','CategoryController@getViewCreate');
Route::get('/list-category','CategoryController@listCategory');
Route::post('/store-category','CategoryController@getCreateCategory');
Route::get('/edit-category/{id}','CategoryController@getEditCategory');
Route::get('/delete-category/{id}','CategoryController@DeleteCategory');
Route::put('/update-category/{id}','CategoryController@UpdateCategory');
/**
 * route tag
 */
Route::get('/view-create-tag','ControllerTag@ViewCreateTag');
Route::post('/create-tag','ControllerTag@CreateTag');
Route::get('/list-tag','ControllerTag@ListTag');
Route::get('/edit-tag/{id}','ControllerTag@EditTag');
Route::put('/update-tag/{id}','Controllertag@Updatetag');
Route::get('delete-tag/{id}','ControllerTag@DeleteTag');

/**
 * route comment
 */
Route::post('/create','CommentController@Create');
Route::get('/list-comment','CommentController@list');
Route::get('/delete-comment/{id}','CommentController@DeleteComment');