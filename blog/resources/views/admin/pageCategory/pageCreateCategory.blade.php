
@extends('admin.mains.main')

@section('content')

            <h2>Create Category</h2>
            <form action="{{ url('store-category') }}" method="POST">
                @csrf
                <label for="fname">Category</label><br>
                <input type="text" id="category" name="name" autocomplete="off"><br>
                <br>

                <button type="submit">Submit</button>
            </form>

@endsection