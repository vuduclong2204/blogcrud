
@extends('admin.mains.main')

@section('content')


            <h2>Edit Category</h2>

            <form action="{{ url('/update-category', $category->id)}}" method="POST">
                @csrf
                @method('PUT')
                <label for="fname">Category</label><br>
                <input type="text" id="category" name="name" value="{{ $category->name }}" autocomplete="off"><br>
                <br>
                <button type="submit">Update</button>
                <br>
            </form>

@endsection