@extends('admin.mains.main')

@section('content')
<div class="textCreate" style="margin-bottom: 10px;">
<h2>Category</h2>

<a href="{{url('/createCategory')}}"  >
  <button>
    <h2>Create</h2>
  </button>
</a><br>
</div>
 @if(session('create'))
            <div class="test">
              {{ session('create') }}
            </div>
  @endif
  

 <div>
@if(session('delete'))
            <div class="test">
              {{ session('delete') }}
            </div>
  @endif
<table action="">
  <tr>
    <th>Category</th>
    <th>Action</th>
  </tr>
  @foreach ($categories as $category) 
  <tr>
    <td>{{ $category->name }}</td>
    <td>

        <button>
          <a href="{{url('/edit-category', $category->slug)}}" >
          Edit
          </a>
        </button>
        
        <button>
          <a href="{{url('/delete-category', $category->id)}}">
          Delete</button>
          </a>
    </td>
  </tr>
  @endforeach
  
</table>
</div>
 


@endsection




