@extends('admin.mains.main')

@section('content')

            <h2>Create Tag</h2>
            <form action="{{ url('create-tag') }}" method="POST">
                @csrf
                <label for="fname">Tag</label><br>
                <input type="text" id="tag" name="name" autocomplete="off"><br>
                <br>

                <button type="submit">Submit</button>
            </form>

@endsection