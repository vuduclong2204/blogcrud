
@extends('admin.mains.main')

@section('content')


            <h2>Edit Category</h2>

            <form action="{{ url('/update-tag', $tag->id)}}" method="POST">
                @csrf
                @method('PUT')
                <label for="fname">Tag</label><br>
                <input type="text" id="tag" name="name" value="{{ $tag->name }}" autocomplete="off"><br>
                <br>
                <button type="submit">Update</button>
                <br>
            </form>

@endsection