@extends('admin.mains.main')

@section('content')
<div class="textCreate" style="margin-bottom: 10px;">
<h2>Tag</h2>

<a href="{{url('/view-create-tag')}}" >
  <button>
    <h2>Create</h2>
  </button>
</a><br>
</div>
 @if(session('create'))
            <div class="test">
              {{ session('create') }}
            </div>
  @endif
  

 <div>
@if(session('delete'))
            <div class="test">
              {{ session('delete') }}
            </div>
  @endif
<table action="">
  <tr>
    <th>#</th>
    <th>Tag</th>
    <th>Action</th>
  </tr>
  @foreach ($tags as $tag) 
  <tr>
    <td>{{ $tag->id}}</td>
    <td>{{ $tag->name }}</td>
    <td>

        <button>
          <a href="{{url('/edit-tag', $tag->id)}}" >
          Edit
          </a>
        </button>
        
        <button>
          <a href="{{url('/delete-tag', $tag->id)}}">
          Delete</button>
          </a>
    </td>
  </tr>
  @endforeach
  
</table>
</div>
 


@endsection




