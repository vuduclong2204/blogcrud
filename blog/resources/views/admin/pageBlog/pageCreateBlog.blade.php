
@extends('admin.mains.main')

@section('content')

            <h2>Create Blog</h2>
            <form action="{{ url('storeBlog') }}" method="POST">
                @csrf
                <label for="fname">Title</label><br>
                <input type="text" id="title" name="title" ><br>
                <br>
                <label for="lname">Content</label><br>
                <input type="text" id="content" name="content" ><br><br>

                <label>Category:</label>
                <select style="width: 200px" name="category_id" >
                    <option>>....Choose category....<</option>
                   @foreach($categories as $category)
                    <option value="{{ $category->id }}">
                        {{ $category->name }}
                    </option>
                   @endforeach
                </select>

                <label>Tag:</label>
                <select style="width: 200px" name="tag_id[]" >
                    <option>>....Choose tag....<</option>
                   @foreach($tags as $tag)
                    <option value="{{ $tag->id }}">
                        {{ $tag->name }}
                    </option>
                   @endforeach
                </select>

                <button type="submit">Submit</button>
            </form>
<script src="{{ asset('assets/js/tiny.js')}}"></script>
@endsection

