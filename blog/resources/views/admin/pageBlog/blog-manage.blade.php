@extends('admin.mains.main')

@section('content')
<div class="textCreate" style="margin-bottom: 10px;">
<h2>Blog</h2>

<a href="{{url('/creatblog')}}" >
  <button>
    <h2>Create</h2>
  </button>
</a><br>
</div>
 @if(session('create'))
            <div class="test">
              {{ session('create') }}
            </div>
  @endif
  

 <div>
@if(session('delete'))
            <div class="test">
              {{ session('delete') }}
            </div>
  @endif
<table action="">
  <tr>
    <th>Title</th>
    <th>Tag</th>
    <th>Category</th>
    <th>Action</th>
  </tr>
  @foreach ($blogs as $blog) 
  <tr>
    <td>{{ $blog->title }}</td>
    <td>
      @foreach($blog->Tags as $tag )
       {{ $tag->name }}, 
      @endforeach
    </td>
    <td>{{ $blog->Category ? $blog->Category->name : '' }}</td>
    <td>

        <button>
          <a href="{{url('/editblog', $blog->slug)}}" >
          Edit
          </a>
        </button>
        
        <button>
          <a href="{{url('/delete',$blog->id)}}">
          Delete
          </a>
          </button>

          <button>
          <a href="{{url('/view-blog',$blog->slug)}}">
          View
          </a>
          </button>
    </td>
  </tr>
  @endforeach
  
</table>
</div>
 
{{$blogs->links()}} 

@endsection




