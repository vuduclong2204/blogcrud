@extends('admin.mains.main')

@section('content')

<div class="blog-view" style="margin-bottom: 10px;">
    
    <div class="cl-title">
       <h1> {{$blog->title}}</h1>
    </div>
    
    <div class="cl-content" >
        <div class="tx-content" name="content">
            {!! $blog->content !!}
        </div>
    </div>
    
<div class="cl-comment">
<form role="form" class="form-comment" action="{{url('create')}}" method="POST">
    @csrf
    <div class="form-group">
        
        <input type="text" name="name" id="name" value="" style="width: 60%;height: 100px;border-color:black" autocomplete="off">
    </div>
    <input type="hidden" name="blog_id" value="{{ $blog->id }}">
<div class="form-group">
    <button  type='comment' style="width: 100px; height: 50px;" >Comment</button>
</div>
        <div class="list-comment">
            @foreach($blog->Comments as $comment )
                <div class="children-comment">
                    {{ $comment->name }}
                    <button>
                        <a href="{{url('/delete-comment',$comment->id)}}">
                        Delete
                        </a>
                </button>

                </div>
                <br>
            @endforeach
        </div>
</div>

</div>

<script src="{{ asset('assets/js/tiny.js')}}"></script>

@endsection


