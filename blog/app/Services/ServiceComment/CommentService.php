<?php

namespace App\Services\ServiceComment;

use App\Repositories\RepositoryComment\CommentRepositoryInterface;
use App\Services\BaseService;

class CommentService extends BaseService
{
    /**
     * Get Repository which need implement in each Repository
     * 
     *  @return void
     */
    public function getRepository()
    {
        return CommentRepositoryInterface::class;
    }

    /**
     * Delete Comment
     * 
     * @param $id ID from DB
     * @return App/Repositories/RepositoryComment/CommentRepositoryInterface
     */
    public function Delete($id)
    {
        return $this->repository->DeleteComment($id);
    }
}