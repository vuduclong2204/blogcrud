<?php

namespace App\Services\ServiceBlog;

use App\Repositories\RepositoryBlog\BlogRepositoryInterface;
use App\Services\BaseService;


class BlogService extends BaseService
{
    /**
     * Get Repository which need implement in each Repository
     * 
     *  @return void
     */
    public function getRepository()
    {
        return BlogRepositoryInterface::class;
    }

    /**
     * Sync Tag Data Blog
     * 
     * @param $blog Blog is created
     * @param $data Data from Request
     * @return App/Repositories/RepositoryBlog/BlogRepositoryInterface
     */
    public function syncTagDataBlog($blog, $data)
    {
        return $this->repository->syncDataTag($blog, $data);
    }

    /**
     * Search blog
     * 
     * @param $data Data from DB
     * @return App/Repositories/RepositoryBlog/BlogRepositoryInterface
     */
    public function SearchBlog($data)
    {
        return $this->repository->SearchBlog($data);
    }

    /**
     * View Blog
     * 
     * @param $slug Data from DB
     * @return App/Repositories/RepositoryBlog/BlogRepositoryInterface
     */
    public function View($slug)
    {
        return $this->repository->View($slug);
    }

    /**
     * Edit Blog
     * 
     * @param
     * @return  App/Repositories/RepositoryBlog/BlogRepositoryInterface
     */
    public function EditBlog($id)
    {
        return $this->repository->EditBlog($id);
    }
}

?>