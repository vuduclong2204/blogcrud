<?php

namespace App\Services\ServiceCategory;

use App\Repositories\RepositoryCategory\CategoryRepositoryInterface;
use App\Services\BaseService;

class CategoryService extends BaseService
{
    /**
     * Get Repository which need implement in each Repository
     * 
     * @return void
     */
    public function getRepository()
    {
        return CategoryRepositoryInterface::class;
    }

    /**
     * EditCategory
     * 
     * @param 
     * @return App/Repositories/RepositoryCategory/CategoryRepositoryInterface
     */
    public function EditCategory($id)
    {
        return $this->repository->EditCategory($id);
    }
}