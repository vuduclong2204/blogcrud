<?php 

namespace App\Services\ServiceTag;

use App\Repositories\RepositoryTag\TagRepositoryInterface;
use App\Services\BaseService;

class TagService extends BaseService
{
    /**
     * Get Repository which need implement in each Repository
     * 
     * @return void
     */
    public function getRepository()
    {
        return TagRepositoryInterface::class;
    }

    /**
     * FindOrFail = id
     * 
     * @param $id Data from DB
     * @return App/Repositories/RepositoryTag/TagRepositoryInterface
     */
    public function FindID($id)
    {
        return $this->repository->FindID($id);
    }

    
}