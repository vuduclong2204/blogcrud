<?php

namespace App\Services;

use App\Repositories\RepositoryInterface\UserRepositoryInterface;


class UserService extends BaseService
{
    /**
     * Implement detail get Repository for each Service
     * 
     * @return void
     */
    public function getRepository()
    {
        return UserRepositoryInterface::class;
    }

    /**
     * Create tags for each user
     * 
     * @param array $attributes
     * @return mixed
     */
    public function getLatest($conds = [])
    {
        return $this->repository->getLatest($conds);
    }
}
?>