<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(
            \App\Repositories\RepositoryBlog\BlogRepositoryInterface::class,
            \App\Repositories\RepositoryBlog\BlogRepository::class
        );

        $this->app->singleton(
            \App\Repositories\RepositoryCategory\CategoryRepositoryInterface::class,
            \App\Repositories\RepositoryCategory\CategoryRepository::class
        );

        $this->app->singleton(
            \App\Repositories\RepositoryTag\TagRepositoryInterface::class,
            \App\Repositories\RepositoryTag\TagRepository::class
        );

        $this->app->singleton(
            \App\Repositories\RepositoryComment\CommentRepositoryInterface::class,
            \App\Repositories\RepositoryComment\CommentRepository::class
        );
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
