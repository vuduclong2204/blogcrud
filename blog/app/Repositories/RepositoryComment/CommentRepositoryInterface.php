<?php

namespace App\Repositories\RepositoryComment;

use App\Repositories\RepositoryInterface;

interface CommentRepositoryInterface extends RepositoryInterface
{
    /**
     * Delete Comment
     * 
     * @param $id ID from DB
     * @return string
     */
    public function DeleteComment($id);
}