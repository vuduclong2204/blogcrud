<?php

namespace App\Repositories\RepositoryComment;

use App\Model\Comments;
use App\Repositories\BaseRepository;

class CommentRepository extends BaseRepository implements CommentRepositoryInterface
{
    /**
     * Implement detail get Model for each Repository
     * 
     * @return void
     */
    public function getModel()
    {
        return Comments::class;
    }

    /**
     * Delete comment
     * 
     * @param $id ID from DB
     * @return string
     */
    public function DeleteComment($id)
    {
        $comment = $this->model->findOrFail($id);

        return $comment;
    }
}