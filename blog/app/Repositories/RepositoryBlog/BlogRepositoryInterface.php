<?php

namespace App\Repositories\RepositoryBlog;

use App\Repositories\RepositoryInterface;

interface BlogRepositoryInterface extends RepositoryInterface
{
    /**
     * Sync Data Tag
     * 
     * @param $blog Blog created
     * @param $data Data from request
     * @return void
     */
    public function syncDataTag($blog, $data);

    /**
     * Sync Data Tag
     * 
     * @param $blog Blog created
     * @param $data Data from request
     * @return void
     */
    public function SearchBlog($data);

    /**
     * View Blog
     * 
     * @param $slug Data from DB
     * @return string
     */
    public function View($slug);

    /**
     * Edit Blog
     * 
     * @param $id ID from DB
     * @return string
     */
    public function EditBlog($id); 
}

?>