<?php

namespace App\Repositories\RepositoryBlog;

use App\Model\tbblog;
use App\Repositories\BaseRepository;

class BlogRepository extends BaseRepository implements BlogRepositoryInterface
{
    /**
     * Implement detail get Model for each Repository
     * 
     * @return void
     */
    public function getModel()
    {
        return tbblog::class;
    }

    /**
     * Sync Data Tag
     * 
     * @param $blog Blog created
     * @param $data Data from request
     * @return Boolean
     */
    public function syncDataTag($blog, $data)
    {
        try {
            $syncTag = $blog->Tags()->sync($data['tag_id']);

            return true;
        } catch (\Throwable $th) {
           return false;
        }
        
    }
    /**
     * View Blog
     * 
     * @param $blog Blog from DB
     * @param $slug Slug from DB
     */
    public function View($slug)
    {
        $blog = $this->model->where('slug', $slug)->with('Comments')->first();

        return $blog;
    }
    
    /**
     * Search Blog
     * 
     * @param $data Data from DB
     * @return array
     */
    public function SearchBlog($data)
    {
        $blogs = $this->model->where('title', 'like', '%' . $data['title'] . '%' )->paginate(config('paginateconfig.paginate_page'));
        
        return $blogs;
    }
    /**
     * Edit Blog
     * 
     * @param $id ID from DB
     * @return string
     */
    public function EditBlog($id)
    {
        $blog = $this->model->where('slug', $id)->first();

        return $blog;
    }
}
?>