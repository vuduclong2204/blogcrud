<?php

namespace App\Repositories\RepositoryCategory;

use App\Repositories\RepositoryInterface;

interface CategoryRepositoryInterface extends RepositoryInterface
{
    /**
     * EditCategory
     * 
     * @param $id ID
     * @return string
     */
    public function EditCategory($id);
}
