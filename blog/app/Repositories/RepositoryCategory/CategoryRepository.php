<?php

namespace App\Repositories\RepositoryCategory;

use App\Model\Categories;
use App\Repositories\BaseRepository;

class CategoryRepository extends BaseRepository implements CategoryRepositoryInterface
{
    /**
     * Implement detail get Model for each Repository
     * 
     * @return void
     */
    public function getModel()
    {
        return Categories::class;
    }

    /**
     * Edit Category
     * 
     * @param $id ID from DB
     * @return string
     */
    public function EditCategory($id)
    {
        $category = $this->model->where('slug',$id)->first();

        return $category;
    }
}