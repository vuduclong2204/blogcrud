<?php

namespace App\Repositories\RepositoryTag;

use App\Model\Tags;
use App\Repositories\BaseRepository;

class TagRepository extends BaseRepository implements TagRepositoryInterface
{
    /**
     * Implement detail get Model for each Repository
     * 
     * @return void
     */
    public function getModel()
    {
        return Tags::class;
    }

    /**
     * FindOrFail = Id
     * 
     * @param $id 
     * @return string
     */
    public function FindID($id)
    {
        $tag = $this->model->findOrFail($id);

        return $tag;
    }

    
}