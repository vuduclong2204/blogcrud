<?php

namespace App\Repositories\RepositoryTag;

use APP\Repositories\RepositoryInterface;
use Illuminate\Contracts\Config\Repository;

interface TagRepositoryInterface extends RepositoryInterface
{
    /**
     * FindOrFail = id
     * 
     * @param 
     * @return string
     */
    public function FindID($id);

    
}