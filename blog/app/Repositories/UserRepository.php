<?php

namespace App\Repositories;

use App\Repositories\BaseRepository;
use App\Repositories\RepositoryInterface\UserRepositoryInterface;


class UserRepository extends BaseRepository implements UserRepositoryInterface
{
    /**
     * Implement detail get Model for each Repository
     * 
     * @return void
     */
    public function getModel()
    {
        return \App\Model\User::class;
    }

    /**
     * Create tags each user
     * 
     * @param array $attributes
     * @return mixed
     */
    public function getLatest($conds =[])
    {
        if(!empty($conds['limit']))  {
            return $this->model->orderBy('update_at','desc')->paginate($conds['limit']);
        }
        return $this->model->orderBy('updated_at','desc')->get();
        
        
    }
}
?>