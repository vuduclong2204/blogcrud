<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Categories extends Model
{
    protected $fillable = [
        'name',
        'slug'
    ];

    /**
     * tao moi quan he
     */
    public function Blogs() 
    {
        return $this->hasMany(tbblog::class);
    }
}
