<?php

namespace App\Model;

use App\Model\Tags;
use App\Model\Comments;
use Illuminate\Database\Eloquent\Model;

class tbblog extends Model
{
    protected $table = 'tbblog';
    protected $fillable = [
        'title',
        'content',
        'slug',
        'category_id'

    ];

/**
 * nghich dao quan he one to many
 */
public function Category()
{
    return $this->belongsTo(Categories::class);
}
/**
 * nghich dao quan he one to many
 */
public function Comments()
{
    return $this->hasMany(Comments::class,'blog_id');
}
/**
 * quan he many to many
 */
public function Tags()
{
    return $this->belongsToMany(Tags::class, 'tbblog_tag', 'tbblog_id', 'tag_id');
}
}



