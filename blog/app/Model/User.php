<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class User extends Model
{
    protected $fillable = [
        'name',
        'email',
        'password',
        'title',
        'gender',
        'avatar_url',
        'education',
        'location',
        'skills',
        'notes',
        'notes',
        'brithday',
        'roles',
        'is_active',
        'remember_token'
    ];
}
