<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Comments extends Model
{
    protected $fillable = [
        'name',
        'blog_id'
    ];
 /**
 * quan he one to many
 */
public function blogs()
{
    return $this->belongsTo(tbblog::class,'blog_id');
}

}
