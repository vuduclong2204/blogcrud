<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Tags extends Model
{
    protected $fillable = [
        'name',
    ];

/**
 * quan he many to many
 */
public function blogs()
{
    return $this->belongsToMany(tbblog::class, 'tbblog_tag', 'tag_id', 'tbblog_id');
}
}
