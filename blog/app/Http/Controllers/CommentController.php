<?php

namespace App\Http\Controllers;

use App\Model\Comments;
use App\Model\tbblog;
use Illuminate\Http\Request;
use App\Services\ServiceComment\CommentService;

class CommentController extends Controller
{

    protected $commentService;
    
    public function __construct(CommentService $commentService)
    {
        $this->commentService = $commentService;
    }
/**
 * post comment
 */
public function Create(Request $request)
{
    $data = $request->all();
    $comment = $this->commentService->create($data);
       
        return redirect()->back();
}
/**
 * delete comment
 */
public function DeleteComment($id)
{
    $comment = Comments::findOrFail($id);
    $comment->delete();

    return redirect()->back();
}
/**
 * list comment
 */
public function list()
{
    $comments = $this->commentService->all();
    
    return view('admin.pageBlog.blog-view');
}
}
