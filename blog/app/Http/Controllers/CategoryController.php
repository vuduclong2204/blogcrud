<?php

namespace App\Http\Controllers;

use App\Http\Requests\CategoryRequest;
use App\Model\Categories;
use App\Services\ServiceCategory\CategoryService;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class CategoryController extends Controller
{
    protected $categoryService;

    public function __construct(CategoryService $categoryService)
    {
        $this->categoryService = $categoryService;
    } 

    /**
     * Form create category 
     */
    public function getViewCreate() 
    {
        return view('admin.pageCategory.pageCreateCategory' ) ;
    } 

    /**
     * list category
     */
    public function listCategory(){
        $categories = $this->categoryService->getAll();
        
        return view('admin.pageCategory.pageListCategory', compact('categories'));
    }
    
    /**
     * create category
     */
    public function getCreateCategory(CategoryRequest $request){
        $data = $request->all();
        $data['slug'] = Str::slug($data['name']);
        $category = $this->categoryService->create($data);

        return redirect()->to('list-category');
    }

    /**
     * edit category
     */
    public function getEditCategory($id){
        $category = $this->categoryService->EditCategory($id);
        
        return view('admin.pageCategory.pageEditCategory',compact('category'));
    }

    /**
     * update category
     */
    public function UpdateCategory(Request $request,$id)
    {
        $data = $request->all();
        $category = $this->categoryService->find($id);
        $category->update($data);
        
        return redirect()->to('list-category');
    }

    /**
     * delete category
     */
    public function DeleteCategory($id)
    {
        $categories = $this->categoryService->find($id);
        $categories->delete();
        
        session()->flash('delete','delete sucess');
        
        return redirect()->to('list-category');
    }
}
