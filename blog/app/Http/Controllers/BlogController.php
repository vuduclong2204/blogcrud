<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\tbblog;
use App\Http\Requests\BlogRequest;
use App\Model\Categories;
use App\Model\Tags;
use Illuminate\Support\Str;
use App\Services\ServiceBlog\BlogService;
use App\Services\ServiceCategory\CategoryService;
use App\Services\ServiceTag\TagService;

class BlogController extends Controller
{

    protected $blogService, $tagService, $categoryService;

    public function __construct(BlogService $blogService, TagService $tagService, CategoryService $categoryService)
    {
        $this->blogService = $blogService;
        $this->tagService = $tagService;
        $this->categoryService = $categoryService;
    }

    /**
     * login 
     */
    public function getLogin(){
        
        return view('admin.pageBlog.pageLogin');
    }
    /**
     * Index blog
     */
    public function getBlog() {
        $blogs = tbblog::paginate(config('paginateconfig.paginate_page'));
        $blogs->load('Category','Tags');  

        return view('admin.pageBlog.blog-manage', compact('blogs'));
    }
    /**
     * view blog
     * @param $slug form DB
     */
    public function ViewBlog($slug)
    {
        $blog = $this->blogService->View($slug);

        return view('admin.pageBlog.blog-view',compact('blog'));
    }
    /**
     * view create
     */
    public function getViewCreateBlog(){
        $tags = $this->tagService->getAll();
        $categories = $this->categoryService->getAll(); 

        return view('admin.pageBlog.pageCreateBlog', compact('categories','tags' ));
    }
    
    /**
     * view profile
     */
    public function getViewProfile(){
        return view('admin.pageBlog.pageProfile');
    }
    /**
     * create blog
     */
    public function getSubmitBlog(BlogRequest $request){
        $data = $request->all();
        $data['slug'] = Str::slug($data['title']);
        $blog = $this->blogService->create($data); 
        // Sync data after created blog with tag id
        $syncTag = $this->blogService->syncTagDataBlog($blog, $data);

        if ($syncTag == true) {
            session()->flash('create','create sucess');
        } else {
            session()->flash('create','Error!!');
        }

        
        return redirect()->route('blogs');
    }
    
    /**
     * edit Blog
     */
    public function getEditBlog($id){
        $categories = $this->categoryService->getAll();
        $tags = $this->tagService->getAll();
        $blog = $this->blogService->EditBlog($id);

        return view('admin.pageBlog.pageEditBlog',compact('blog','categories','tags'));
    }
    /**
     * update blog
     */
    public function getUpdateBlog(Request $request,$id){
        
        $data = $request->all();
        $blog = $this->blogService->find($id);
        $blog = $this->blogService->update($id,$data);
        // Sync data after update blog with tag id
        $syncTag = $this->blogService->syncTagDataBlog($blog, $data); 
        if ($syncTag == true) {
            session()->flash('create','create sucess');
        } else {
            session()->flash('create','Error!!');
        }
   
        return redirect()->route('blogs');
    }
    /**
     * search blog
     */
    public function getSearch(Request $request){
        $data = $request->all();

        $blogs = $this->blogService->SearchBlog($data);
         
        return view('admin.pageBlog.blog-manage', compact('blogs'));
    }
    /**
     * delete blog
     */
    
    public function getDeleteBlog($id){

        $blog = $this->blogService->find($id);
        $blog->delete();
        //flash delete
        session()->flash('delete','delete sucess');
        
        return redirect()->route('blogs');
    }  
}
