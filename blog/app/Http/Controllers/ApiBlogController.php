<?php

namespace App\Http\Controllers;

use App\Http\Requests\BlogRequest;
use Illuminate\Http\Request;
use App\Model\tbblog;
use Illuminate\Http\Response;
use Illuminate\Support\Str;

class ApiBlogController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $article = tbblog::paginate(config('paginateconfig.paginate_page'));

        return response()->json($article);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(BlogRequest $request)
    {
        $data = $request->all();
        //
        $blog = tbblog::create([
            'title' => $data['title'],
            'content' => $data['content'],
            'slug'=> Str::slug($data['title'])
        ]);

        return response()->json($blog, 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {
        $data = $request->all();

        $blogs = tbblog::where('title', 'like', '%' . $data['title'] . '%' )->paginate(config('paginateconfig.paginate_page'));

        return response()->json($blogs, 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
       
        $data = $request->all();
        $blog = tbblog::findOrFail($id);

        $blog->update($data);

        return response()->json($blog, 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $blog = tbblog::findOrFail($id);
        $blog->delete();

        return json([null, 204]);
    }
}
