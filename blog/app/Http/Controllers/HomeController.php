<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use App\Model\tbblog;


class HomeController extends Controller
{
    public function getIndex() {
        return view("admin.mains.main");
    }

    /**
     * Index blog
     */
    public function getBlog() {
        $blogs = tbblog::all();

        return view('admin.pages.pageCreateblog', compact('blogs'));
    }

    /**
     * get table
     */
  
}