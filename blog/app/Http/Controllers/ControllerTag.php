<?php

namespace App\Http\Controllers;

use App\Model\Tags;
use Illuminate\Http\Request;
use App\Services\ServiceTag\TagService;


class ControllerTag extends Controller
{
    protected $tagService;

    public function __construct(TagService $tagService)
    {
        $this->tagService = $tagService;
    }
/**
 * view create
 */
public function ViewCreateTag()
{   
    return view('admin.pageTag.pageCreateTag' );
}
/**
 * list tag
 */
public function ListTag()
{
    $tags = $this->tagService->getAll();
    
    return view('admin.pageTag.pageListTag',compact('tags'));
}
/**
 * create Tag
 */
public function CreateTag(Request $request)
{
    $data = $request->all();
    $tag = $this->tagService->create($data);

    return redirect()->to('list-tag');
}
/**
 * EditTag
 */
public function EditTag($id)
{
    $tag = $this->tagService->FindID($id);
   
    return view('admin.pageTag.pageEditTag',compact('tag'));
}
/**
 * update tag
 */
public function UpdateTag(Request $request, $id)
{
    $data = $request->all();
    $tag = $this->tagService->FindID($id);
    $tag->update($data);

    return redirect()->to('list-tag');
}
/**
 * delete tag
 */
public function DeleteTag($id)
{
    $tag = $this->tagService->FindID($id);
    $tag->delete();

    return redirect()->to('list-tag');
}
}
